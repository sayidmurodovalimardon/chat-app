package uz.infinityandro.chatapp.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import uz.infinityandro.chatapp.R
import uz.infinityandro.chatapp.shared.AppPreference
import uz.infinityandro.chatapp.util.Constants

public open class BaseActivity : AppCompatActivity() {
    var database1 = FirebaseFirestore.getInstance()
    lateinit var document: DocumentReference
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppPreference.init(this)
        document = database1.collection(Constants.KEY_COLLECTION_USER)
            .document(AppPreference.userId!!)
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
    }

    override fun onPause() {
        super.onPause()
        document.update(Constants.KEY_AVAILABILITY,0)
    }

    override fun onResume() {
        super.onResume()
        document.update(Constants.KEY_AVAILABILITY,1)
    }
}