package uz.infinityandro.chatapp.activities

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import android.os.Bundle
import android.util.Base64
import android.view.View
import androidx.appcompat.app.AppCompatDelegate
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import uz.infinityandro.chatapp.adapter.ChatAdapter
import uz.infinityandro.chatapp.databinding.ActivityChatsBinding
import uz.infinityandro.chatapp.model.ChatMessage
import uz.infinityandro.chatapp.model.User
import uz.infinityandro.chatapp.network.ApiClient
import uz.infinityandro.chatapp.network.ApiService
import uz.infinityandro.chatapp.shared.AppPreference
import uz.infinityandro.chatapp.util.Constants
import uz.infinityandro.chatapp.util.displayToast
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class ChatsActivity : BaseActivity() {
    private lateinit var binding: ActivityChatsBinding
    private lateinit var adapter: ChatAdapter
    var list = ArrayList<ChatMessage>()
    lateinit var receiver: User
    var database = FirebaseFirestore.getInstance()
    var conversionId: String? = null
    var isReceiverAvailable = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityChatsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        AppPreference.init(this)
        loadUser()
        init()
        listener()
        listenMessage()
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)


    }

    fun init() {
        adapter = ChatAdapter(list, getUserImage(receiver.image), AppPreference.userId!!)
        binding.chatRecyclerView.adapter = adapter
        database = FirebaseFirestore.getInstance()
    }

    private fun sendMessage() {
        var hash = HashMap<String, Any>()
        hash.put(Constants.KEY_SENDER_ID, AppPreference.userId!!)
        hash.put(Constants.KEY_RECEIVER_ID, receiver.id)
        hash.put(Constants.KEY_MESSAGE, binding.inputMessage.text.toString())
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            hash.put(Constants.KEY_TIMESTAMP, Date())
        }
        database.collection(Constants.KEY_COLLECTION_CHAT)
            .add(hash)
        if (conversionId != null) {
            updateConversation(binding.inputMessage.text.toString())
        } else {
            var convertState = HashMap<String, Any>()
            convertState.put(Constants.KEY_SENDER_ID, AppPreference.userId!!)
            convertState.put(Constants.KEY_SENDER_NAME, AppPreference.name!!)
            convertState.put(Constants.KEY_SENDER_IMAGE, AppPreference.image!!)
            convertState.put(Constants.KEY_RECEIVER_ID, receiver.id)
            convertState.put(Constants.KEY_RECEIVER_NAME, receiver.name)
            convertState.put(Constants.KEY_RECEIVER_IMAGE, receiver.image)
            convertState.put(Constants.KEY_LAST_MESSAGE, binding.inputMessage.text.toString())
            convertState.put(Constants.KEY_TIMESTAMP, Date())
            addConvertStation(convertState)
        }
        if (!isReceiverAvailable){
            try {
                var tokens=JSONArray()
                tokens.put(receiver.token)

                var data=JSONObject()
                data.put(Constants.KEY_USER_ID,AppPreference.userId)
                data.put(Constants.KEY_NAME,AppPreference.name)
                data.put(Constants.KEY_IMAGE_FCM_TOKEN,AppPreference.token)
                data.put(Constants.KEY_MESSAGE,binding.inputMessage.text.toString())

                var body=JSONObject()
                body.put(Constants.REMOTE_MSG_DATA,data)
                body.put(Constants.REMOTE_MSG_REGISTRATION_IDS,tokens)

                sendNotification(body.toString())

            }catch (e:Exception){

            }
        }else{

        }
        binding.inputMessage.setText(null)

    }

    private fun listenAvailabilityOfReceiver() {
        database.collection(Constants.KEY_COLLECTION_USER).document(receiver.id)
            .addSnapshotListener { value, error ->
                if (error != null) {
                    return@addSnapshotListener;
                }
                if (value != null) {
                    if (value.getLong(Constants.KEY_AVAILABILITY) != null) {
                        var availablity =
                            Objects.requireNonNull(value.getLong(Constants.KEY_AVAILABILITY))
                                ?.toInt()
                        isReceiverAvailable = availablity == 1
                    }
                    receiver.token = value?.getString(Constants.KEY_IMAGE_FCM_TOKEN)!!

                }
                if (isReceiverAvailable) {
                    binding.networkChanges.setText("Online")
                } else {
                    binding.networkChanges.setText("Offline")
                }
            }
    }

    private fun sendNotification(message: String) {
        ApiClient.getApi().create(ApiService::class.java)
            .sendMessage(Constants.getRemoteMsgHeaders(), message)
            .enqueue(object : Callback<String> {
                override fun onResponse(call: Call<String>, response: Response<String>) {
                    if (response.isSuccessful) {
                        try {
                            if (response.body() !=null){
                                val responseJson=JSONObject(response.body())
                                val results=responseJson.getJSONArray("results")
                                if (responseJson.getInt("failure")==1){
                                    val error=results.get(0) as JSONObject
                                    displayToast(error.toString())
                                    return
                                }
                            }

                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                        displayToast("Notification send successfully")
                    } else {
                        displayToast("Error+ ${response.code()}")
                    }
                }

                override fun onFailure(call: Call<String>, t: Throwable) {
displayToast(t.localizedMessage)
                }

            })


    }

    private fun listenMessage() {
        database.collection(Constants.KEY_COLLECTION_CHAT)
            .whereEqualTo(Constants.KEY_SENDER_ID, AppPreference.userId)
            .whereEqualTo(Constants.KEY_RECEIVER_ID, receiver.id)
            .addSnapshotListener(event)
        database.collection(Constants.KEY_COLLECTION_CHAT)
            .whereEqualTo(Constants.KEY_SENDER_ID, receiver.id)
            .whereEqualTo(Constants.KEY_RECEIVER_ID, AppPreference.userId)
            .addSnapshotListener(event)
    }

    private val event = EventListener<QuerySnapshot> { value, error ->
        if (error != null) {
            return@EventListener
        }
        if (value != null) {
            var count = list.size
            value.documentChanges.forEach { document ->
                if (document.type == DocumentChange.Type.ADDED) {

                    var senderId = document.document.getString(Constants.KEY_SENDER_ID)
                    var receiverId = document.document.getString(Constants.KEY_RECEIVER_ID)
                    var message = document.document.getString(Constants.KEY_MESSAGE)
                    var dateTime = getReadData(document.document.getDate(Constants.KEY_TIMESTAMP)!!)
                    var dateObject = document.document.getDate(Constants.KEY_TIMESTAMP)
                    var user =
                        ChatMessage(senderId!!, receiverId!!, message!!, dateTime, dateObject!!)
                    list.add(user)
                }
            }
            list.sortWith(Comparator { obj1, obj2 ->
                obj1.date!!.compareTo(obj2.date)
            })
            if (count == 0) {
                adapter.notifyDataSetChanged()
            } else {
                adapter.notifyItemRangeInserted(list.size, list.size)
                binding.chatRecyclerView.smoothScrollToPosition(list.size - 1)

            }

        }
        binding.progressBar.visibility = View.GONE
        if (conversionId.isNullOrEmpty()) {
            checkConversation()
        }
    }


    private fun getUserImage(s: String): Bitmap {
        val byte = Base64.decode(s, Base64.DEFAULT)
        return BitmapFactory.decodeByteArray(byte, 0, byte.size)
    }

    private fun listener() {
        binding.imageBack.setOnClickListener {
            onBackPressed()
        }
        binding.sendImage.setOnClickListener {
            sendMessage()
        }
    }

    private fun loadUser() {
        receiver = intent.getSerializableExtra(Constants.KEY_USER) as User
        binding.textName.text = receiver.name
    }

    private fun getReadData(data: java.util.Date): String {
        return SimpleDateFormat("MMMM dd, yyyy-hh:mm a", Locale.getDefault()).format(data)
    }

    private fun updateConversation(message: String) {
        var documentReference =
            database.collection(Constants.KEY__COLLECTION_CONVERSATIONS).document(conversionId!!)
        documentReference.update(
            Constants.KEY_LAST_MESSAGE,
            message,
            Constants.KEY_TIMESTAMP,
            Date()
        )
    }

    private fun addConvertStation(conversation: HashMap<String, Any>) {
        database.collection(Constants.KEY__COLLECTION_CONVERSATIONS)
            .add(conversation)
            .addOnSuccessListener {
                conversionId = it.id
            }
    }

    private fun checkConversation() {
        if (list.size != 0) {
            checkForConversationRemotely(
                AppPreference.userId!!,
                receiver.id
            )
            checkForConversationRemotely(
                receiver.id,
                AppPreference.userId!!
            )

        }
    }

    private fun checkForConversationRemotely(senderId: String, receiverId: String) {
        database.collection(Constants.KEY__COLLECTION_CONVERSATIONS)
            .whereEqualTo(Constants.KEY_SENDER_ID, senderId)
            .whereEqualTo(Constants.KEY_RECEIVER_ID, receiverId)
            .get()
            .addOnCompleteListener(conversationCompletedListener)

    }

    private val conversationCompletedListener = OnCompleteListener<QuerySnapshot> { task ->
        if (task.isSuccessful && task.result != null && task.result!!.documents.size > 0) {
            var snap = task.result!!.documents.get(0)
            conversionId = snap.id
        }
    }

    override fun onResume() {
        super.onResume()
        listenAvailabilityOfReceiver()
    }
}