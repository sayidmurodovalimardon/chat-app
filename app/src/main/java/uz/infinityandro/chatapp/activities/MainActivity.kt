package uz.infinityandro.chatapp.activities

import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Base64
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import com.google.firebase.firestore.*
import com.google.firebase.messaging.FirebaseMessaging
import uz.infinityandro.chatapp.adapter.RecentOCnversationAdapter
import uz.infinityandro.chatapp.databinding.ActivityMainBinding
import uz.infinityandro.chatapp.model.ChatMessage
import uz.infinityandro.chatapp.shared.AppPreference
import uz.infinityandro.chatapp.util.Constants
import uz.infinityandro.chatapp.util.displayToast

class MainActivity : BaseActivity() {
    private lateinit var binding: ActivityMainBinding
    var databse = FirebaseFirestore.getInstance()
    lateinit var adapter: RecentOCnversationAdapter
    lateinit var list: ArrayList<ChatMessage>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        AppPreference.init(this)
        init()
        loadUser()
        getToken()
        listener()
        listenConversation()
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
    }

    private fun init() {
        list = ArrayList()
        adapter = RecentOCnversationAdapter(list) { user ->
            Intent(this, ChatsActivity::class.java)?.let {
                it.putExtra(Constants.KEY_USER, user)
                startActivity(it)
            }
        }
        binding.ConversationRecyclerView.adapter = adapter


    }

    private fun listenConversation() {
        databse.collection(Constants.KEY__COLLECTION_CONVERSATIONS)
            .whereEqualTo(Constants.KEY_SENDER_ID, AppPreference.userId)
            .addSnapshotListener(event)

        databse.collection(Constants.KEY__COLLECTION_CONVERSATIONS)
            .whereEqualTo(Constants.KEY_RECEIVER_ID, AppPreference.userId)
            .addSnapshotListener(event)
    }

    private val event = EventListener<QuerySnapshot> { value, error ->
        if (error != null) {
            return@EventListener
        }
        if (value != null) {
            value.documentChanges.forEach { documentChange ->
                if (documentChange.type == DocumentChange.Type.ADDED) {

                    var senderId = documentChange.document.getString(Constants.KEY_SENDER_ID)
                    var receiverId = documentChange.document.getString(Constants.KEY_RECEIVER_ID)
                    if (AppPreference.userId.equals(senderId)) {
                        var conversionImage =
                            documentChange.document.getString(Constants.KEY_RECEIVER_IMAGE)
                        var conversionName =
                            documentChange.document.getString(Constants.KEY_RECEIVER_NAME)
                        var conversionId =
                            documentChange.document.getString(Constants.KEY_RECEIVER_ID)
                        var message = documentChange.document.getString(Constants.KEY_LAST_MESSAGE)
                        var dateObj = documentChange.document.getDate(Constants.KEY_TIMESTAMP)
                        var chatMessage = ChatMessage(
                            senderId!!,
                            receiverId!!,
                            message!!,
                            "",
                            dateObj,
                            conversionId,
                            conversionName,
                            conversionImage
                        )
                        list.add(chatMessage)
                    } else {
                        var conversionImage =
                            documentChange.document.getString(Constants.KEY_SENDER_IMAGE)
                        var conversionName =
                            documentChange.document.getString(Constants.KEY_SENDER_NAME)
                        var conversionId =
                            documentChange.document.getString(Constants.KEY_SENDER_ID)
                        var message = documentChange.document.getString(Constants.KEY_LAST_MESSAGE)
                        var dateObj = documentChange.document.getDate(Constants.KEY_TIMESTAMP)
                        var chatMessage = ChatMessage(
                            senderId!!,
                            receiverId!!,
                            message!!,
                            "",
                            dateObj,
                            conversionId,
                            conversionName,
                            conversionImage
                        )

                        list.add(chatMessage)
                    }


                } else if (documentChange.type == DocumentChange.Type.MODIFIED) {
                    list.forEach { chat ->

                    }
                    for (i in 0..list.size step 1) {
                        var senderId = documentChange.document.getString(Constants.KEY_SENDER_ID)
                        var receiverId =
                            documentChange.document.getString(Constants.KEY_RECEIVER_ID)
                        if (list[i].senderId.equals(senderId) && list[i].receiverId.equals(
                                receiverId
                            )
                        ) {
                            list[i].message =
                                documentChange.document.getString(Constants.KEY_LAST_MESSAGE)!!
                            list[i].date = documentChange.document.getDate(Constants.KEY_TIMESTAMP)
                            break
                        }
                    }

                }
            }
            list.sortWith(Comparator { o1, o2 ->
                o2.date!!.compareTo(o1.date)
            })
            adapter.notifyDataSetChanged()
            binding.ConversationRecyclerView.smoothScrollToPosition(0)
            binding.recentProgress.visibility = View.GONE

        }

    }

    private fun listener() {
        binding.powerOff.setOnClickListener {
            signOut()
        }
        binding.floatingActionButton.setOnClickListener {
            Intent(this, UserActivity::class.java)?.let {
                startActivity(it)
            }
        }
    }

    private fun loadUser() {
        binding.name.text = AppPreference.name
        val byte = Base64.decode(AppPreference.image, Base64.DEFAULT)
        val bitmap = BitmapFactory.decodeByteArray(byte, 0, byte.size)
        binding.imageProfile.setImageBitmap(bitmap)
    }

    private fun getToken() {
        FirebaseMessaging.getInstance().token.addOnSuccessListener(this::updateToken)
    }

    private fun updateToken(s: String) {
        AppPreference.token=s
        var reference =
            databse.collection(Constants.KEY_COLLECTION_USER).document(AppPreference.userId!!)
        reference.update(Constants.KEY_IMAGE_FCM_TOKEN, s)
            .addOnSuccessListener {
            }
            .addOnFailureListener {
            }
    }

    private fun signOut() {
        displayToast("Signing out...")
        val document = databse.collection(Constants.KEY_COLLECTION_USER)
            .document(AppPreference.userId.toString())
        var hashMap = HashMap<String, Any>()
        hashMap.put(Constants.KEY_IMAGE_FCM_TOKEN, FieldValue.delete())
        document.update(hashMap)
            .addOnSuccessListener {
                AppPreference.clear()
                Intent(this, SignInActivity::class.java)?.let {
                    startActivity(it)
                }
                finish()
            }
            .addOnFailureListener {
                displayToast("Unable to sign out")
            }
    }
}