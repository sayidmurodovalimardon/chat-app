package uz.infinityandro.chatapp.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.view.View
import androidx.appcompat.app.AppCompatDelegate
import com.google.firebase.firestore.FirebaseFirestore
import uz.infinityandro.chatapp.R
import uz.infinityandro.chatapp.databinding.ActivitySignInBinding
import uz.infinityandro.chatapp.shared.AppPreference
import uz.infinityandro.chatapp.util.Constants
import uz.infinityandro.chatapp.util.displayToast

class SignInActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySignInBinding
    private var database = FirebaseFirestore.getInstance()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppPreference.init(this)
        binding = ActivitySignInBinding.inflate(layoutInflater)
        setContentView(binding.root)
        if (AppPreference.firstRun){
            Intent(this,MainActivity::class.java)?.let {
                startActivity(it)
            }
        }
        listener()
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)

    }

    private fun listener() {
        binding.newAccount.setOnClickListener {
            Intent(this, SignUpActivity::class.java).let {
                startActivity(it)
            }


        }
        binding.buttonSignIn.setOnClickListener {
            if (checkGaps()){
                signIn()
            }
        }
    }

    private  fun signIn(){
        loading(true)
        database.collection(Constants.KEY_COLLECTION_USER)
            .whereEqualTo(Constants.KEY_EMAIL,binding.inputEmail.text.toString())
            .whereEqualTo(Constants.KEY_PASSWORD,binding.inputPassword.text.toString())
            .get()
            .addOnCompleteListener {task->
                if (task.isSuccessful && task.getResult()!=null && task.result!!.documents.size>0){
                    val document= task.result!!.documents[0]
                    AppPreference.firstRun=true
                    AppPreference.name=document[Constants.KEY_NAME].toString()
                    AppPreference.image=document[Constants.KEY_IMAGE].toString()
                    AppPreference.userId=document.id
                    Intent(this,MainActivity::class.java)?.let {
                        startActivity(it)
                    }
                }else{
                    loading(false)
                    displayToast("Unable to sign in")
                }

            }

    }

    private fun loading(s:Boolean){
    if (s){
        binding.buttonSignIn.visibility=View.GONE
        binding.progressbar.visibility=View.VISIBLE
    }else{
        binding.buttonSignIn.visibility=View.VISIBLE
        binding.progressbar.visibility=View.INVISIBLE
    }
    }

    private fun checkGaps():Boolean{
        if (binding.inputEmail.text.isNullOrEmpty()) {
            displayToast("Enter your email")
            return false
        } else if (binding.inputPassword.text.isNullOrEmpty()) {
            displayToast("Enter your password")
            return false
        } else if (!Patterns.EMAIL_ADDRESS.matcher(binding.inputEmail.text.toString().trim())
                .matches()
        ) {
            displayToast("Enter valid email")
            return false
        }
        return true
    }




}