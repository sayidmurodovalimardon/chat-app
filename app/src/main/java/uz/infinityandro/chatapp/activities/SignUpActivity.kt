package uz.infinityandro.chatapp.activities

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Base64
import android.util.Base64.encodeToString
import android.util.Patterns
import android.view.View
import androidx.appcompat.app.AppCompatDelegate
import com.github.dhaval2404.imagepicker.ImagePicker
import org.koin.androidx.viewmodel.ext.android.viewModel
import uz.infinityandro.chatapp.R
import uz.infinityandro.chatapp.databinding.ActivitySignUpBinding
import uz.infinityandro.chatapp.mvvm.SignUpViewModel
import uz.infinityandro.chatapp.shared.AppPreference
import uz.infinityandro.chatapp.util.Constants
import uz.infinityandro.chatapp.util.displayToast
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.util.*
import kotlin.collections.HashMap

class SignUpActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySignUpBinding
    lateinit var image: Any
    private val viewModel: SignUpViewModel by viewModel()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySignUpBinding.inflate(layoutInflater)
        setContentView(binding.root)
        listener()
        observers()
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
    }

    private fun getInputStreamFromUri(uri: Uri): InputStream? {
        return applicationContext.contentResolver.openInputStream(uri)
    }

    private fun observers() {
        viewModel.uiState.observe(this, {
            val data = it ?: return@observe
            data.error?.let {
                displayToast(it)
            }
        })
        viewModel.ozgar.observe(this,{
            binding.buttonSignUp.visibility=if (it) View.VISIBLE else View.GONE
            binding.signUpProgress.visibility=if (!it) View.VISIBLE else View.GONE
        })
    }

    private fun listener() {
        binding.addImage.setOnClickListener {
            ImagePicker.with(this)
                .crop()                    //Crop image(Optional), Check Customization for more option
                .compress(1024)            //Final image size will be less than 1 MB(Optional)
                .maxResultSize(
                    1080,
                    1080
                )    //Final image resolution will be less than 1080 x 1080(Optional)
                .start()
        }

        binding.buttonSignUp.setOnClickListener {
            if (image == null
            ) {
                displayToast("Fill all gaps")
            } else if (binding.inputEmail.text.isNullOrEmpty()) {
                displayToast("Enter your email")
            } else if (binding.inputName.text.isNullOrEmpty()) {
                displayToast("Enter your name")
            } else if (binding.inputPassword.text.isNullOrEmpty()) {
                displayToast("Enter your password")
            } else if (binding.inputPasswordConfirm.text!!.equals(binding.inputPassword.text)) {
                displayToast("Confirm your password")
            } else if (!Patterns.EMAIL_ADDRESS.matcher(binding.inputEmail.text.toString().trim())
                    .matches()
            ) {
                displayToast("Enter valid email")
            } else {
                var hashMap = HashMap<String, Any>()
                hashMap.put(Constants.KEY_NAME, binding.inputName.text.toString())
                hashMap.put(Constants.KEY_EMAIL, binding.inputEmail.text.toString())
                hashMap.put(Constants.KEY_PASSWORD, binding.inputPassword.text.toString())
                hashMap.put(Constants.KEY_IMAGE, image)
                viewModel.saveData(hashMap)
                Intent(this, MainActivity::class.java)?.let {
                    startActivity(it)
                }

            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            val uri: Uri = data?.data!!
            binding.image.setImageURI(uri)
            binding.addText.visibility = View.GONE
            getInputStreamFromUri(uri)?.let { inputStream ->
                val orginalBitmap = BitmapFactory.decodeStream(inputStream)
                val width = applicationContext.resources.displayMetrics.widthPixels
                val height = ((orginalBitmap.height * width) / orginalBitmap.width)
                val bitmap = Bitmap.createScaledBitmap(orginalBitmap, width, height, false)
                val outputStream = ByteArrayOutputStream()
                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, outputStream)
                val byte = outputStream.toByteArray()
                image = Base64.encodeToString(byte, android.util.Base64.DEFAULT)
            }

        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            displayToast(ImagePicker.getError(data))
        } else {
            displayToast("Task Cancelled")
        }
    }
}