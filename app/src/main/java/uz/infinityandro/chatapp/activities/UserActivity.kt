package uz.infinityandro.chatapp.activities

import android.content.Intent
import android.icu.lang.UCharacter
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.firestore.FirebaseFirestore
import uz.infinityandro.chatapp.adapter.UserAdapter
import uz.infinityandro.chatapp.databinding.ActivityUserBinding
import uz.infinityandro.chatapp.model.User
import uz.infinityandro.chatapp.shared.AppPreference
import uz.infinityandro.chatapp.util.Constants
import uz.infinityandro.chatapp.util.displayToast

class UserActivity : BaseActivity() {
    private lateinit var binding: ActivityUserBinding
    var database = FirebaseFirestore.getInstance()
    lateinit var adapter: UserAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUserBinding.inflate(layoutInflater)
        setContentView(binding.root)
        AppPreference.init(this)
        getUsers()
        setListener()
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
    }
    private fun setListener(){
        binding.imageBack.setOnClickListener {
            onBackPressed()
        }
    }


    private fun getUsers() {
        loading(true)
        database.collection(Constants.KEY_COLLECTION_USER)
            .get()
            .addOnCompleteListener { task ->
                loading(false)
                var userId = AppPreference.userId
                if (task.isSuccessful && task.getResult() != null) {
                    var list = ArrayList<User>()
                    task.getResult()!!.forEach { document ->
                        if (userId.equals(document.id)) {
                            do {
                                continue
                            } while (false)
                        }
                        val name = document[Constants.KEY_NAME].toString()
                        val email = document[Constants.KEY_EMAIL].toString()
                        val image = document[Constants.KEY_IMAGE].toString()
                        val token = document[Constants.KEY_IMAGE_FCM_TOKEN].toString()
                        val id=document.id

                        var user = User(name, image, email, token,id)
                        list.add(user)

                    }
                    if (list.size > 0) {
                        adapter = UserAdapter(list){user->
                            Intent(this,ChatsActivity::class.java)?.let {
                                it.putExtra(Constants.KEY_USER,user)
                                startActivity(it)
                                finish()
                            }
                        }

                        binding.userRecyclerView.adapter = adapter
                        binding.userRecyclerView.addItemDecoration(DividerItemDecoration(this,LinearLayoutManager.VERTICAL))
                    } else {
                        showErrorMesage()
                    }
                } else {
                    showErrorMesage()
                }
            }
    }

    private fun showErrorMesage() {
        binding.error.text = "No user available"
        binding.error.visibility = View.VISIBLE
    }

    private fun loading(isLoading: Boolean) {
        if (isLoading) {
            binding.progressBar.visibility = View.VISIBLE
        } else {
            binding.progressBar.visibility = View.GONE

        }
    }
}