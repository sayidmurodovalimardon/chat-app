package uz.infinityandro.chatapp.adapter

import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import uz.infinityandro.chatapp.databinding.ItemContainerReceivedMessageBinding
import uz.infinityandro.chatapp.databinding.ItemContainerSendMessageBinding
import uz.infinityandro.chatapp.model.ChatMessage

class ChatAdapter(var list: List<ChatMessage>, var bitmap: Bitmap, var senderId: String) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    val VIEW_TYPE_SENT = 1
    val VIEW_TYPE_RECEIVED = 2

    inner class VH(var binding: ItemContainerSendMessageBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun setData(chatMessage: ChatMessage) {
            binding.textMessage.text = chatMessage.message
            binding.date.text = chatMessage.dataTime
        }
    }

    inner class VH1(var binding: ItemContainerReceivedMessageBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun setData(chatMessage: ChatMessage, bitmap: Bitmap) {
            binding.textMessage.text = chatMessage.message
            binding.textDateTime.text = chatMessage.dataTime
            binding.imageProfile.setImageBitmap(bitmap)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (list[position].senderId.equals(senderId)) {
            VIEW_TYPE_SENT
        } else {
            VIEW_TYPE_RECEIVED
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        if (viewType == VIEW_TYPE_SENT) {

            return VH(
                ItemContainerSendMessageBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        } else {
            return VH1(
                ItemContainerReceivedMessageBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (getItemViewType(position) == VIEW_TYPE_SENT) {
            (holder as VH).setData(list[position])
        }else{
            (holder as VH1).setData(list[position],bitmap)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }
}