package uz.infinityandro.chatapp.adapter

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import uz.infinityandro.chatapp.databinding.ItemContainerRecentConversationBinding
import uz.infinityandro.chatapp.model.ChatMessage
import uz.infinityandro.chatapp.model.User

class RecentOCnversationAdapter(var list: List<ChatMessage>,var listener:(user:User)->Unit):RecyclerView.Adapter<RecentOCnversationAdapter.VH> (){
    inner class VH(var itemContainerRecentConversationBinding: ItemContainerRecentConversationBinding):RecyclerView.ViewHolder(itemContainerRecentConversationBinding.root){
        fun bind(chatMessage: ChatMessage) {
            itemContainerRecentConversationBinding.imageProfile.setImageBitmap(getUserImage(
                chatMessage.conversionImage!!
            ))
            itemContainerRecentConversationBinding.textName.setText(chatMessage.conversionName)
            itemContainerRecentConversationBinding.textRecentCOnversation.setText(chatMessage.message)
            itemContainerRecentConversationBinding.root.setOnClickListener {
                var id=chatMessage.conversionId.toString()
                var name=chatMessage.conversionName.toString()
                var image=chatMessage.conversionImage.toString()
                var user=User(name,image,"","",id)
                listener(user)
            }

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(ItemContainerRecentConversationBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }

    private fun getUserImage(s: String): Bitmap {
        val byte = Base64.decode(s, Base64.DEFAULT)
        return BitmapFactory.decodeByteArray(byte, 0, byte.size)
    }
}