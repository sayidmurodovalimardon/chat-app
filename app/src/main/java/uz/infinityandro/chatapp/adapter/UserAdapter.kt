package uz.infinityandro.chatapp.adapter

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import uz.infinityandro.chatapp.databinding.ItemContainerUserBinding
import uz.infinityandro.chatapp.model.User

class UserAdapter(var list: List<User>,var listener:(user:User)->Unit) : RecyclerView.Adapter<UserAdapter.VH>() {
    inner class VH(val itemContainerUserBinding: ItemContainerUserBinding) :
        RecyclerView.ViewHolder(itemContainerUserBinding.root) {
        fun bind(user: User) {
            itemContainerUserBinding.imageProfile.setImageBitmap(getUserImage(user.image))
            itemContainerUserBinding.textEmail.text=user.email
            itemContainerUserBinding.textName.text=user.name
            itemContainerUserBinding.root.setOnClickListener {
                listener(user)
            }
        }

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(
            ItemContainerUserBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(list[position])
    }

    override fun getItemCount(): Int = list.size

    private fun getUserImage(s: String): Bitmap {
        val byte = Base64.decode(s, Base64.DEFAULT)
        return BitmapFactory.decodeByteArray(byte, 0, byte.size)
    }
}