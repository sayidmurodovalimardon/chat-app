package uz.infinityandro.chatapp.dagger

import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import uz.infinityandro.chatapp.repository.SignUpRepository
import uz.infinityandro.chatapp.repository.SignUpRepositoryImpl

val repositoryModule = module {
    factory<SignUpRepository> { SignUpRepositoryImpl(androidContext()) }
}