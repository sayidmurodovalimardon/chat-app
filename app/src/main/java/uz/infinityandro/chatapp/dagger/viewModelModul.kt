package uz.infinityandro.chatapp.dagger

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import uz.infinityandro.chatapp.mvvm.SignUpViewModel

val viewModelModul= module {
val viewModel = viewModel { SignUpViewModel(repository = get()) }
}