package uz.infinityandro.chatapp.model

import java.io.Serializable
import java.util.*

data class ChatMessage(
    var senderId: String,
    var receiverId: String,
    var message: String,
    var dataTime: String,
    var date: Date? = null,
    var conversionId:String? =null,
    var conversionName:String? =null,
    var conversionImage:String? =null
) :Serializable{

}
