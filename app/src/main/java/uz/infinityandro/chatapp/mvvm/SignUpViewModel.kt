package uz.infinityandro.chatapp.mvvm

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import uz.infinityandro.chatapp.repository.SignUpRepository
import uz.infinityandro.chatapp.util.Coroutine
import java.io.Serializable

class SignUpViewModel(private val repository: SignUpRepository) : ViewModel() {
    var savedObserver = MutableLiveData<SavedImagesUres>()
    val uiState: LiveData<SavedImagesUres> get() = savedObserver
    var ozgar=MutableLiveData<Boolean>()
    fun saveData(user: HashMap<String, Any>) {
        Coroutine.io {
            kotlin.runCatching {
                emitUiState(isLoading = true)
                repository.saveDataToFireStore(user,ozgar)
            }.onSuccess {

            }.onFailure {
                emitUiState(error = it.message.toString())
            }
        }
    }


    private fun emitUiState(
        isLoading: Boolean = false,
        error: String? = null

    ) {
        val dataState = SavedImagesUres(isLoading, error)
        savedObserver.postValue(dataState)
    }

    data class SavedImagesUres(
        val isLoading: Boolean,
        val error: String?
    ):Serializable
}