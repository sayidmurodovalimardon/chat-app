package uz.infinityandro.chatapp.network

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ApiClient {
    var retrofit: Retrofit? = null
    fun getApi():Retrofit{
  retrofit=Retrofit.Builder()
      .addConverterFactory(GsonConverterFactory.create())
      .baseUrl("https://fcm.googleapis.com/fcm/")
      .build()

        return retrofit!!
    }
}