package uz.infinityandro.chatapp.repository

import android.net.Uri
import androidx.lifecycle.MutableLiveData

interface SignUpRepository {
    suspend fun saveDataToFireStore(user: HashMap<String, Any>, ozgar:MutableLiveData<Boolean>)
}