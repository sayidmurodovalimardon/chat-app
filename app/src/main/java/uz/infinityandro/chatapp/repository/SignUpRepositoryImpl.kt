package uz.infinityandro.chatapp.repository

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import androidx.lifecycle.MutableLiveData
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase
import uz.infinityandro.chatapp.shared.AppPreference
import uz.infinityandro.chatapp.util.Constants
import java.io.InputStream

class SignUpRepositoryImpl(private val context: Context):SignUpRepository {
    var database=FirebaseFirestore.getInstance()
    override suspend fun saveDataToFireStore(user: HashMap<String, Any>,ozgar:MutableLiveData<Boolean>) {
        ozgar.postValue(false)
            database.collection(Constants.KEY_COLLECTION_USER)
                .add(user)
                .addOnSuccessListener {
                    ozgar.postValue(true)
                    AppPreference.init(context)
                    AppPreference.image=user[Constants.KEY_IMAGE].toString()
                    AppPreference.name=user[Constants.KEY_NAME].toString()
                    AppPreference.userId=it.id
                }
                .addOnFailureListener {

                }
        }
    }

