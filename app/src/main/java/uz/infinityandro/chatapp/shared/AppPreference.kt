package uz.infinityandro.chatapp.shared

import android.content.Context
import android.content.SharedPreferences
import uz.infinityandro.chatapp.util.Constants

object AppPreference {
    private const val NAME = "SpinKotlin"
    private const val MODE = Context.MODE_PRIVATE
    private lateinit var preferences: SharedPreferences

    fun init(context: Context) {
        preferences = context.getSharedPreferences(NAME, MODE)
    }

    private inline fun SharedPreferences.edit(operation: (SharedPreferences.Editor) -> Unit) {
        val editor = edit()
        operation(editor)
        editor.apply()
    }


    var firstRun: Boolean
        get() = preferences.getBoolean(Constants.KEY_IS_SIGNED_IN, false)
        set(value) = preferences.edit {
            it.putBoolean(
                Constants.KEY_IS_SIGNED_IN, value
            )
        }

    var name: String?
        get() = preferences.getString(Constants.KEY_NAME,"")
        set(value) = preferences.edit {
            it.putString(
                Constants.KEY_NAME, value
            )
        }

    var image: String?
        get() = preferences.getString(Constants.KEY_IMAGE,"")
        set(value) = preferences.edit {
            it.putString(
                Constants.KEY_IMAGE, value
            )
        }


    var userId: String?
        get() = preferences.getString(Constants.KEY_USER_ID,"")
        set(value) = preferences.edit {
            it.putString(
                Constants.KEY_USER_ID, value
            )
        }

    var token: String?
        get() = preferences.getString(Constants.KEY_IMAGE_FCM_TOKEN,"")
        set(value) = preferences.edit {
            it.putString(
                Constants.KEY_IMAGE_FCM_TOKEN, value
            )
        }
    fun clear(){
        preferences.edit().clear().commit()
    }
}