package uz.infinityandro.chatapp.util

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import uz.infinityandro.chatapp.dagger.repositoryModule
import uz.infinityandro.chatapp.dagger.viewModelModul

@Suppress("unused")
class AppConfig:Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@AppConfig)
            modules(listOf(repositoryModule, viewModelModul))
        }

    }
}